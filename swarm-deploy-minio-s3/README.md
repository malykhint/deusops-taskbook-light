[ Light ]
1. Развернуть виртуальную машину с Linux. Можно использовать любые хостинги и способы создания, в том числе и локально на Vagrant
2. Установить Docker и запустить Minio из стандартного образа
3. Освоить Docker-secret, создать для Minio access key и secret key внутри Docker-secret
4. Создать 4 виртуальных машины (как в п.1), освоить работу Docker Swarm и включить все машины в общий кластер Docker Swarm
5. Развернуть Minio через Docker Stack в кластере Docker Swarm
6. Освоить dockerfile-инструкцию Healthcheck и добавить проверку в Docker Stack

[ Normal ]
1. Написать Ansible Playbook для развертывания docker на 4 нодах
2. Написать Ansible Playbook для развертывания кластера Docker Swarm на 4 нодах, вынести в отдельную роль атомарные операции по типу установки Docker и создание кластера Docker Swarm
3. Создать отдельную Ansible-роль для развертывания Minio через Swarm Stack
4. Учесть в ролях создание Docker-secrets, автоматические ip-адреса узлов, вынести все настраиваемые параметры в переменные
5. Сделать jinja-шаблон для Stack-файла, который динамически формируется с учетом количества нод и их настроек
6. Покрыть роли тестами через molecule и настроить подключение и версионирование через личный galaxy в Gitlab

[ Hard ]
1. Развернуть Minio standalone в облачном Kubernetes
2. Развернуть в Kubernetes Minio-operator

id 230103
