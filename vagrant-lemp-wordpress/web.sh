#!/bin/bash
SITENAME="wordpress.local"
ROOTPATH="/var/www/wordpress"
DOCUMENTROOT="$ROOTPATH"
MUSER="wordpressuser"
MPASSWORD="wordpresspassword"
MDATABASE="wordpress"
MHOST="192.168.56.100"

### Update packages
sudo apt update -y
### Install and configure nginx
sudo apt-get install -y nginx unzip
sudo cp /vagrant_data/nginx/wordpress /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/wordpress /etc/nginx/sites-enabled/
rm -rf /etc/nginx/sites-enabled/default
wget https://wordpress.org/latest.zip
mkdir -p /var/www/wordpress
sudo unzip latest.zip -d /var/www/
sudo chown -R www-data:www-data /var/www/wordpress/
sudo systemctl enable nginx
sudo systemctl restart nginx
sudo cp $DOCUMENTROOT/wp-config-sample.php $DOCUMENTROOT/wp-config.php
sudo sed -i "s/database_name_here/$MDATABASE/g" $DOCUMENTROOT/wp-config.php
sudo sed -i "s/username_here/$MUSER/g" $DOCUMENTROOT/wp-config.php
sudo sed -i "s/password_here/$MPASSWORD/g" $DOCUMENTROOT/wp-config.php
sudo sed -i "s/localhost/$MHOST/g" $DOCUMENTROOT/wp-config.php

### Install and configure PHP-FPM
sudo apt-get install -y php-cli php-json php-pdo php-mysql php-mbstring php-fpm php-curl php-gd php-intl php-mbstring php-soap php-xml php-xmlrpc php-zip
systemctl restart php7.4-fpm
