#!/bin/bash

MUSER="wordpressuser"
MPASSWORD="wordpresspassword"
MDATABASE="wordpress"
MROOTPASSWORD="123456789"

### update packages
sudo apt-get update -y

### Install and configure MySQL
sudo apt-get -y install mysql-server
#mysql -u root -prootpass -e "UPDATE mysql.user SET authentication_string=PASSWORD('$MROOTPASSWORD') WHERE User='root'; flush privileges;"

mysql -u root -prootpass -e "CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
#mysql -u root -prootpass -e "GRANT ALL ON wordpress.* TO 'wordpressuser'@'localhost' IDENTIFIED BY 'wordpresspassword';"
mysql -u root -prootpass -e "CREATE USER 'wordpressuser'@'%' IDENTIFIED BY 'wordpresspassword';"
mysql -u root -prootpass -e "GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpressuser'@'%';"
mysql -u root -prootpass -e "FLUSH PRIVILEGES;"

sudo sed -i "s/127.0.0.1/::/" /etc/mysql/mysql.conf.d/mysqld.cnf
sudo service mysql restart

#sudo cp /vagrant_data/mysql/.my.cnf /home/ubuntu/
#sudo sed -i "s/rootpassword/$MROOTPASSWORD/g" /home/ubuntu/.my.cnf
#sudo chmod 400 /home/ubuntu/.my.cnf
#sudo chown ubuntu:ubuntu /home/ubuntu/.my.cnf

sudo cp /vagrant_data/mysql/.my.cnf /root/
sudo sed -i "s/rootpassword/$MROOTPASSWORD/g" /root/.my.cnf
sudo chmod 400 /root/.my.cnf
sudo chown root:root /root/.my.cnf

sudo /usr/bin/mysql_secure_installation << EOF
n
n
y
y
y
y
EOF

#mysql -u root -prootpass -e "CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
#mysql -u root -prootpass -e "GRANT ALL ON wordpress.* TO 'wordpressuser'@'localhost' IDENTIFIED BY 'wordpresspassword';"
#mysql -u root -prootpass -e "FLUSH PRIVILEGES;"
