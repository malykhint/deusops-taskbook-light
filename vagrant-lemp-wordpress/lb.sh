#!/bin/bash

### Update packages
sudo apt update -y
### Install and configure nginx
sudo apt-get install -y nginx unzip
sudo cp /vagrant_data/nginx/lb /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/lb /etc/nginx/sites-enabled/
rm -rf /etc/nginx/sites-enabled/default
sudo systemctl restart nginx; sudo systemctl enable nginx
